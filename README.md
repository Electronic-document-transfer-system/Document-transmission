# 电子公文传输系统

#### 介绍
{**以下是 Gitee 平台说明，您可以替换此简介**
Gitee 是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用 Gitee 实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)





电子公文传输系统网站分为网站首页、公文列表、公文在线观看、公文批改、公文搜索和用户管理

建议直接放到D盘下运行，否则使用绝对路径的会报错

安全性措施有：

- 身份认证（用户名-口令）
  - 对口令做了加盐的sha-256哈希保护用户的个人隐私
- 访问控制（只能访问不大于自己密级的文件）
  - 低密级的用户的页面中不存在高密级的文件
  - 低密级的用户试图通过路由猜测文件的位置会被返回失败并记录日志
- [防御跨站脚本攻击（XSS）](https://docs.djangoproject.com/zh-hans/3.2/topics/security/#cross-site-scripting-xss-protection)
  - 转义特殊字符来防御XSS攻击：<转义为&lt， >转义为&gt， ’转义为&#x27 “转义为&quot ，& 转义为&amp
- [防御跨站点请求伪造（CSRF）](https://docs.djangoproject.com/zh-hans/3.2/topics/security/#cross-site-request-forgery-csrf-protection)
  - 通过检查每一个 POST 请求中的密文来实现。这保证恶意用户不能“复现”一个表单并用 POST 提交到你的网页，并让一个已登录用户无意中提交该表单。恶意用户必须知道特定于用户的密文（使用 cookie）
- [防御 SQL 注入](https://docs.djangoproject.com/zh-hans/3.2/topics/security/#sql-injection-protection)
  - 查询的 SQL 代码与查询的参数是分开定义的。参数可能来自用户从而不安全，因此它们由底层数据库引擎进行转义。
- [防御访问劫持](https://docs.djangoproject.com/zh-hans/3.2/topics/security/#clickjacking-protection)
  - 在框架或 iframe 中加载资源。如果响应包含值为 `SAMEORIGIN` 的头，那么只有当请求来自同一个网站时，浏览器才会在框架中加载资源
- [SSL/HTTPS](https://docs.djangoproject.com/zh-hans/3.2/topics/security/#ssl-https)
  - 恶意用户不能在客户端和服务器之间嗅探验证资格或其他信息
- [Host 头部验证](https://docs.djangoproject.com/zh-hans/3.2/topics/security/#host-header-validation)
  - 当 `DEBUG`为``True`` 和 `ALLOWED_HOSTS` 为空时，主机将根据 `['.localhost', '127.0.0.1', '[::1]']` 进行验证，防止非预期的访问
- [Referrer 策略](https://docs.djangoproject.com/zh-hans/3.2/topics/security/#referrer-policy)
  - 保护用户隐私
- [会话安全](https://docs.djangoproject.com/zh-hans/3.2/topics/security/#session-security)
  - 要求一个被部署的网页应让不受信任的用户不能访问任何子域
- 限流（？？？）
- 限制用户访问的流量，防止DOS攻击
- 文件加密传输
- 用非对称密钥(SM2)保护对称密钥，再用对称密钥实现对文件的加解密
- 文件安全性校验
- 检查文件二进制格式，防止PE文件冒名传输

实现的功能有：

  - 用户注册登录

  - 文件在线查看

  - 文件上传、下载

  - 文件批改

需要安装的模块

```
pip3 install filetype
pip3 install django-sslserver
pip3 install gmssl
pip3 install secrets
pip3 install PyPDF2
```

https启动方法：

```
python manage.py runsslserver 
--certificate D:\ProgramData\Anaconda3\Lib\site-packages\sslserver\certs\development.crt 
--key D:\ProgramData\Anaconda3\Lib\site-packages\sslserver\certs\development.key
```

抓包结果（已经够https加密）：

![image-20210605224546570](https://i.loli.net/2021/06/05/VM2rJSexf4v5DwT.png)